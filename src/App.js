import './App.css';
import Login from './pages/Login/Login'
import Home from './components/Home'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Nav from './components/Nav/Navbar'

function App() {
  return (
    <BrowserRouter>
    <Nav/>
    <Switch>
      <Route path='/' exact component={Login}/>
      <Route path='/Feed' component={Home}/>
      </Switch>
    </BrowserRouter> 
  );
}

export default App;
