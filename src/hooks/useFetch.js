import {useEffect, useState} from 'react'
import axios from 'axios'

export const urlFeeds = 'http://167.99.162.146/feeds/';

export const useFetch = (token) => {
    let config = {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      }

      const bodyParameters = {
        "url": "http://contenidos.lanacion.com.ar/herramientas/rss-origen=2"
     };

    const [state, setState] = useState({data: {}, loading: true, error: ''})

    useEffect(() => {
      
            
            axios.get( urlFeeds,bodyParameters, config )
        
            .then((response) => {
                setState({
                    data: {
                        news: response.data                        
                    },
                    loading: false, 
                    error: ''
                   })
              
                console.log("SUCCESS", response);
            })
            .catch((error) => {
                console.log("ERROR", error);
            });
    }, []);
    return state
}
