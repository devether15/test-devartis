import React, { useContext} from 'react';
import styled from 'styled-components';
import {ContextUser} from '../../context/UserContext'

const Ul = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;

  li {
    padding: 18px 10px;
  }

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    background-color: #0D2538;
    position: fixed;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 0;
    right: 0;
    height: 100vh;
    width: 300px;
    z-index:1;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;

    li {
      color: #fff;
    }
  }
`;

const RightNav = ({ open }) => {
  const {logout, logged} = useContext(ContextUser)
  
  return (
    <Ul className="ulLinks" open={open}>
      <li>Home</li>
      <li>About Us</li>
      {logged? null : <li>Register</li>}
      {logged? <li onClick={logout}>Sign Up</li> : null}
    </Ul>
  )
}

export default RightNav
