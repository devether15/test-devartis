import React, { useState, useEffect } from 'react'
import {useFetch} from '../hooks/useFetch'

export const ContextUser = React.createContext(null); 

export const ContextUserProvider = (props) => {
    
   const [stateLog, setStateLog] = useState({
       user: null,
       logged: false,
       token: '',
       id: null
   })

   const {logged, user, token} = stateLog

  const values = [stateLog, setStateLog]
  // console.log(stateLog)
  
  useEffect(() => {
    const data = localStorage.getItem('usuario')
    if(data){
      setStateLog(JSON.parse(data))
    }
  }, [])

  useEffect(() => {
    localStorage.setItem('usuario', JSON.stringify(stateLog))
  });

  const logout = () => setStateLog({user: null, logged: false, toke:'', id:null})

  const context = useFetch(token)

  return (
    <ContextUser.Provider value={{stateLog, setStateLog, logout, logged, user}}>
      {props.children}
    </ContextUser.Provider>
  );
};