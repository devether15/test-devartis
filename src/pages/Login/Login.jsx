import { Field,  Form, Formik } from 'formik';
import React, { useContext, useState} from 'react';
import { Fragment } from 'react';
import { Link, useHistory } from 'react-router-dom';
import * as Yup from 'yup'
import 'bootstrap/dist/css/bootstrap.min.css';
import './login.css'
import axios from 'axios';
import { ContextUser } from '../../context/UserContext';




function Login(props) {

  const { className} = props
 
   
  const {stateLog, setStateLog} = useContext(ContextUser)



  const history = useHistory()

  const [errorMessage, setErrorMessage] = useState(null)

  const onSubmit = values => {

    const urlLogin = 'http://167.99.162.146/users/login'
   
      const configRequest = {
        'user': values.user,
        'password': values.password
      }

      axios.post(urlLogin,configRequest)
                                       .then(response => {
                                          console.log(response.data)
                                          setStateLog(
                                            {user: response.data,
                                            logged: true,
                                            token: response.data.access_token,
                                            id: response.data.user_id
                                            }
                                          )

                                          history.push('/Feed')
                                       })
                                       .catch(error => {
                                          console.log(error);
                                          // console.log(configRequest)
                                          setErrorMessage(true)})
                                         
    
  }


  const initialValues = {
    user: '',
    password: ''
  }
  const validationSchema = Yup.object({
    user: Yup.string().required(''),
    password: Yup.string().required('')
  })

    return (
        <Fragment>
   
     

          <Formik initialValues={initialValues}
                  onSubmit={onSubmit}
                  validationSchema={validationSchema}>
                {({errors,
                 touched,
                  isValid: isvalid})=> (
   
            <div className='d-flex'> 
            <div className='login-container'>        
                <Form className="needs-validation no-border" noValidate>
                <div className="form-group">
                  <label htmlFor="user">User</label>
                  <Field type="user" className="form-control" name='user' id="password" aria-describedby="userHelp" placeholder="Ingresá tu user" required/>
                </div>
                <div className="form-group">
                  <label htmlFor="password">Contraseña</label>
                  <Field type="password" name='password' className="form-control" id="password" placeholder="Ingresà tu contaseña" required/>
                
              {errorMessage && <p className='text-danger'>Usuario o contraseña invalidos. Intente de nuevo</p>}
                </div>
                <div className="col text-center">
                <button  disabled={!isvalid} type="submit" className="btn btn-primary rounded-pill px-5 py-2 mt-3">INGRESAR</button>
            </div>
            </Form>
            </div> 
            </div> 
          )}
        </Formik>
   
     </Fragment>
  )
}

export default Login
